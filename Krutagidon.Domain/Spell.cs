﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using Krutagidon.Common;

namespace Krutagidon.Domain
{
	public class Spell
	{
		public Spell(
			string name, 
			string description,
			SpellLifetime lifetime,
			int? lifetimeDuration = null,
			int? probability = null,
			List<SpellAdditionalConditions>? additionalConditions = null)
		{
			Name = name;
			Description = description;

			if (lifetime != SpellLifetime.Once && lifetimeDuration is null && lifetimeDuration < 1)
			{
				throw new Exception("Lifetime duration is can not be null or 0!");
			}

			if (probability > SpellConstants.MaxProbability)
			{
				throw new Exception($"Probability > {SpellConstants.MaxProbability}!");
			}
			
			Lifetime = lifetime;
			LifetimeDuration = lifetimeDuration;
			Probability = probability;
			
			if (additionalConditions?.Any(spellAdditionalConditions => !spellAdditionalConditions.ConditionsForDescription.Any()) == true)
			{
				throw new Exception("Conditions for description is empty!");
			}

			AdditionalConditions = additionalConditions ?? new List<SpellAdditionalConditions>();
		}
		
		public Spell(
			string name, 
			string description,
			int probability,
			List<SpellAdditionalConditions>? additionalConditions = null)
		{
			Name = name;
			Description = description;

			if (probability > SpellConstants.MaxProbability)
			{
				throw new Exception($"Probability > {SpellConstants.MaxProbability}!");
			}
			
			Lifetime = SpellLifetime.Once;
			Probability = probability;
			
			if (additionalConditions?.Any(spellAdditionalConditions => !spellAdditionalConditions.ConditionsForDescription.Any()) == true)
			{
				throw new Exception("Conditions for description is empty!");
			}

			AdditionalConditions = additionalConditions ?? new List<SpellAdditionalConditions>();
		}
		
        public string Name { get; set; }

        public string Description { get; set; }

        public SpellLifetime Lifetime { get; set; }

        public int? LifetimeDuration { get; set; }
        
        public int? Probability { get; }
        
        public List<SpellAdditionalConditions> AdditionalConditions { get; set; }
    }
}


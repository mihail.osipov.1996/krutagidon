using System.Collections.Generic;

namespace Krutagidon.Domain
{
    public class SpellAdditionalConditions
    {
        public SpellAdditionalConditions()
        {
            ConditionsForName = new List<string>();
            ConditionsForDescription = new List<string>();
        }
        
        public List<string> ConditionsForName { get; set; }
        
        public List<string> ConditionsForDescription { get; set; }
    }
}
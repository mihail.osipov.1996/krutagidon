﻿using System;

namespace Krutagidon.Domain
{
    public enum SpellLifetime
    {
        Once,
        Circle,
        Round,
        Constant
    }
}
using System.Collections.Generic;
using Krutagidon.Domain;

namespace Krutagidon.Resources
{
    public static class SpellsResources
    {
        public static List<Spell> Spells { get; } = new List<Spell>()
        {
            new Spell("Фаталити", "Все игроки умирают", 70),
            new Spell("Что-то с чем-то",
                "Все игроки берут случайную карту с типом {0} на {1} ходов затем уничтожают ее",
                100,
                new List<SpellAdditionalConditions>()
                {
                    new SpellAdditionalConditions()
                    {
                        ConditionsForDescription = new List<string>()
                        {
                            "Тварь",
                            "Сокровище"
                        }
                    },
                    new SpellAdditionalConditions()
                    {
                        ConditionsForDescription = new List<string>()
                        {
                            "3",
                            "4",
                            "5"
                        }
                    }
                }),
            new Spell("Что-то с чем-то {0}",
                "Все игроки берут случайную карту с типом {0} на {1} ходов затем уничтожают ее",
                100,
                new List<SpellAdditionalConditions>()
                {
                    new SpellAdditionalConditions()
                    {
                        ConditionsForName = new List<string>()
                        {
                            "Test1",
                            "Test2"
                        },
                        ConditionsForDescription = new List<string>()
                        {
                            "Тварь",
                            "Сокровище"
                        }
                    },
                    new SpellAdditionalConditions()
                    {
                        ConditionsForDescription = new List<string>()
                        {
                            "3",
                            "4",
                            "5"
                        }
                    }
                })
        };
    }
}
﻿#nullable enable
using System.Collections.Generic;
using Krutagidon.Common;
using Krutagidon.Domain;

namespace Krutagidon.Services
{
	public static class RandomSpellService
	{
		private static int? CurrentRandomNumber { get; set; }

		public static Spell? GetRandomSpell(this List<Spell> spells)
		{
			CurrentRandomNumber = GetRandomNumber(spells.Count);
			return spells[CurrentRandomNumber.Value].GetSpellWithProbability().BuildFullSpell();
		}

		private static int GetRandomNumber(int count)
		{
			if (CurrentRandomNumber == null)
			{
				return RandomService.GetRandomNumber(count);
			}

			var randomNumber = RandomService.GetRandomNumber(count);
			return randomNumber == CurrentRandomNumber ? GetRandomNumber(count) : randomNumber;
		}

		private static Spell? GetSpellWithProbability(this Spell spell)
		{
			var probabilityRandomNumber = RandomService.GetRandomNumber(SpellConstants.MaxProbability);
			return probabilityRandomNumber > spell.Probability ? null : spell;
		}
	}
}


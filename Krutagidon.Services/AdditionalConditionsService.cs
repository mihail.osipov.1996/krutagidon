#nullable enable
using System.Collections.Generic;
using System.Linq;
using Krutagidon.Domain;

namespace Krutagidon.Services
{
    public static class AdditionalConditionsService
    {
        public static Spell? BuildFullSpell(this Spell? spell)
        {
            if (spell == null)
                return null;

            if (!spell.AdditionalConditions.Any()) 
                return spell;
            
            var forName = new List<string>();
            var forDescription = new List<string>();

            foreach (var spellAdditionalConditions in spell.AdditionalConditions)
            {
                if (spellAdditionalConditions.ConditionsForName.Any())
                {
                    forName.Add(spellAdditionalConditions.ConditionsForName.GetRandomItem());
                }

                forDescription.Add(spellAdditionalConditions.ConditionsForDescription.GetRandomItem());
            }

            if (forName.Any())
                spell.Name = string.Format(spell.Name, args: forName);
                
            spell.Description = string.Format(spell.Description, args: forDescription);

            return spell;
        }
    }
}
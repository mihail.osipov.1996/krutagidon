using System;
using Krutagidon.Domain;

namespace Krutagidon.Services
{
    public static class SpellLifetimeTranslateService
    {
        public static string GetDisplayedName(this SpellLifetime spellLifetime, int? spellLifetimeDuration)
        {
            return spellLifetime switch
            {
                SpellLifetime.Once => "один раз",
                SpellLifetime.Circle => "круг" + (spellLifetimeDuration!.Value > 1 ? "а" : ""),
                SpellLifetime.Round => "раунд" + (spellLifetimeDuration!.Value > 1 ? "а" : ""),
                SpellLifetime.Constant => "постоянно",
                _ => throw new ArgumentOutOfRangeException(nameof(spellLifetime), spellLifetime, null)
            };
        }
    }
}
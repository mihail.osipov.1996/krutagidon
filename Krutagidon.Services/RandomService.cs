using System.Collections.Generic;
using System.Linq;
using Java.Util;

namespace Krutagidon.Services
{
    public static class RandomService
    {
        public static int GetRandomNumber(int max)
        {
            var random = new Random();
            var randomNumber = random.NextInt(max);

            return randomNumber;
        }
        
        public static T GetRandomItem<T>(this List<T> collection)
        {
            var random = new Random();
            var randomNumber = random.NextInt(collection.Count());

            return collection[randomNumber];
        }
    }
}